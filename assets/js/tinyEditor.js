import 'tinymce';
import tinymce from "tinymce";

document.addEventListener('DOMContentLoaded', (event) => {
   const foundEditors = document.querySelectorAll('[data-type="tinyEditor"]');
   foundEditors.forEach((element) => {
       //TODO: make it universal (add and read options from data-option-*)
       tinymce.init({
           selector: element.dataset.tinyOptionSelector ?? ('#'+element.id)
       })
   })
});