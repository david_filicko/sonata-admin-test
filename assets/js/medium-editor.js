import MediumEditor from 'medium-editor';


document.addEventListener('DOMContentLoaded', (event) => {
    const DIV_EDITOR_IDENTIFIER = "[data-mediumeditor-linked-textarea-id]";
    const editableDivs = document.querySelectorAll(DIV_EDITOR_IDENTIFIER);

    editableDivs.forEach((element) => {
        const textAreaId = element.dataset.mediumeditorLinkedTextareaId;
        const textArea = document.getElementById(textAreaId);

        element.innerHTML = unescape(element.innerText);

        element.addEventListener('input', () => {
            textArea.value = element.innerHTML
        })
    });

    const editor = new MediumEditor(DIV_EDITOR_IDENTIFIER);
})