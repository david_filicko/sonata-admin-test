<?php


namespace App\Admin;


use App\Entity\Article;
use App\Entity\Tag;
use App\Form\Type\MediumTextEditor;
use App\Form\Type\TinyEditorType;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Form\Type\CollectionType;
use Sonata\AdminBundle\Form\Type\ModelAutocompleteType;
use Sonata\AdminBundle\Form\Type\ModelType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\ColorType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;

class ArticleAdmin extends AbstractAdmin
{
    protected $baseRouteName = "article";
    protected $baseRoutePattern = "article";

    protected function configureFormFields(FormMapper $form)
    {
        $form
            ->add("title", TextType::class)
            ->add("body", TinyEditorType::class)
            ->add("tag", ModelAutocompleteType::class, [
                "minimum_input_length" => 1,
                "property" => 'tag',
                "multiple" => true
                ])
        ;
    }

    protected function configureListFields(ListMapper $list)
    {
        $list->add("title");
    }

    public function toString($object)
    {
        $text = parent::toString($object);;
        if($object instanceof Article)
        {
            $text = $object->getTitle();
        }
        return $text;
    }
}