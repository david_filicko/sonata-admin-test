<?php


namespace App\Admin;


use App\Entity\Tag;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class TagAdmin extends AbstractAdmin
{
    protected $baseRouteName = "tag";
    protected $baseRoutePattern = "tag";

    protected function configureFormFields(FormMapper $form)
    {
        $form->add("tag", TextType::class);
    }

    protected function configureDatagridFilters(DatagridMapper $filter)
    {
        $filter->add("tag");
    }

    protected function configureListFields(ListMapper $list)
    {
        $list->addIdentifier("tag");
    }
}