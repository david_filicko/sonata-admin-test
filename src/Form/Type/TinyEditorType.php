<?php


namespace App\Form\Type;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class TinyEditorType extends AbstractType
{

    public function getBlockPrefix(): string
    {
        return 'tiny_editor';
    }

    public function getName(): string
    {
        return $this->getBlockPrefix();
    }

    public function getParent()
    {
        return TextareaType::class;
    }

}