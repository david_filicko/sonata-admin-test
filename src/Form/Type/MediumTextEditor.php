<?php


namespace App\Form\Type;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class MediumTextEditor extends TextType
{

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
//            'template' => '@App/Form/Type/medium_text_editor.html.twig',
        ]);
    }

    public function getBlockPrefix(): string
    {
        return 'medium_text_editor';
    }

    public function getName(): string
    {
        return $this->getBlockPrefix();
    }

    public function getParent()
    {
        return TextareaType::class;
    }
}